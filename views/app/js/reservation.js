
function insertarActualizarReserva(idForm){

		    var formData = new FormData($(idForm)[0]);

            var ruta = "ajax.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta").html(datos);
                }
            });			

}

function mostrarServiciosProveedor(){

	var proveedor = $('#proveedor_r').val();
	datos="transaccion=mostrar_servicios_proveedor&proveedor_reserva="+proveedor;
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{
		    $("#lista_servicios_proveedor").html(datos);
		}
	});	
}

function alCargarDocumento(){
	
	document.querySelector('#alta-reserva').addEventListener("submit", function(evento){
		evento.preventDefault();
		insertarActualizarReserva("#alta-reserva");
	});
	$('#proveedor_r').change(mostrarServiciosProveedor)

 }

window.addEventListener("load",alCargarDocumento)