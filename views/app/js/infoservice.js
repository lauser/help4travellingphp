function seleccionarServicio(e){
	e.preventDefault();
	var proveedor = e.target.dataset.proveedor;
	var servicio = e.target.dataset.servicio;

	datos = "transaccion=detalle_servicio&proveedor="+proveedor+"&servicio="+servicio;	
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{
		    $("#respuesta").html(datos);
		    $(window).scrollTop(0);
		}
	});		

}


function alCargarDocumento(){

	var linksSeleccionarServicio = document.querySelectorAll(".seleccionar-servicio");


	for ( var i = 0 ; i < linksSeleccionarServicio.length ; i++ ){

		linksSeleccionarServicio[i].addEventListener("click", seleccionarServicio);

	};
 }

window.addEventListener("load",alCargarDocumento)