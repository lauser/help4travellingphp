function eliminarReserva(evento){

	evento.preventDefault();
	var nroAutoGenerado = evento.target.dataset.numero;
	datos = "transaccion=eliminar_reserva&nroAuto="+nroAutoGenerado;
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{	location.reload();
		    $("#respuesta").html(datos);			
		}
	});			
}

function alCargarDocumento(){

	var linksEliminarReserva = document.querySelectorAll(".eliminar-reserva");


	for ( var i = 0 ; i < linksEliminarReserva.length ;i++ ){

		linksEliminarReserva[i].addEventListener("click", eliminarReserva);

	};	
 
 }

window.addEventListener("load",alCargarDocumento)