
var respuesta = document.querySelector("#respuesta");
var cantCliksProveedor = 0;

function registroUser(e){

		e.preventDefault();
	    var formData = new FormData($("#alta-usuario")[0]);

	    if ($("#proveedor_opt").prop("checked")) {
	    	formData.append("esProveedor", true);
		}

        var ruta = "ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
                $("#respuesta").html(datos);
            }
        });	

}

function mostrarCampo(id_opt, id, type, name, cantClicks, label_text){

			if (cantClicks == 0 ) {
				var padre = document.querySelector(id_opt);

				var label = document.createElement('LABEL');
				var texto = document.createTextNode(label_text);
				label.setAttribute("for", id);
				label.appendChild(texto);
				var item = document.createElement('INPUT');
				item.setAttribute("id",id);
				item.setAttribute("type", type);
				item.setAttribute("name",name);


				var br = document.createElement('br');
				padre.appendChild(br);			
				padre.appendChild(label)
				padre.appendChild(item);

			}

			if (cantClicks % 2 == 0 ) {
			    document.querySelector("#"+id).style.display = "block";
			}
			else{
				document.querySelector("#"+id).style.display = "none";
				

			}	
		}

function alCargarDocumento(){
	
	document.querySelector('#alta-usuario').addEventListener("submit", registroUser);
	document.querySelector('#proveedor_opt').addEventListener("click",
					function(){							
						mostrarCampo("#contenedor_proveedor_link","pagina_web", "text", "pagina_web_txt", cantCliksProveedor,"link");
						mostrarCampo("#contenedor_proveedor_empresa","empresa", "text", "empresa_txt", cantCliksProveedor, "empresa");
						cantCliksProveedor++;
				});	

 }

window.addEventListener("load",alCargarDocumento)


