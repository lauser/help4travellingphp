function seleccionarReserva(e){
	e.preventDefault();
	var nroAutoGenerado = e.target.dataset.numero;
	datos = "transaccion=detalle_reserva&nroAuto="+nroAutoGenerado;
	
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{
		    $("#respuesta").html(datos);
		    $(window).scrollTop(0);
		}
	});		

}


function alCargarDocumento(){

	var linksSeleccionarReserva = document.querySelectorAll(".seleccionar-reserva");


	for ( var i = 0 ; i < linksSeleccionarReserva.length ; i++ ){

		linksSeleccionarReserva[i].addEventListener("click", seleccionarReserva);

	};	

 }

window.addEventListener("load",alCargarDocumento)