function verDetalleUsuario(transaccion, nickname){

	datos = "transaccion="+transaccion+"&nickname="+nickname;
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{
		    $("#respuesta").html(datos);
		    $(window).scrollTop(0);
		}
	});		

}
						
function alCargarDocumento(){

	var linksDetalleProveedor = document.querySelectorAll(".detalle-proveedor");

	for ( var i = 0 ; i < linksDetalleProveedor.length ;i++ ){

		linksDetalleProveedor[i].addEventListener("click",function(e){
			e.preventDefault();
			var nickname = e.target.dataset.nickname;
			verDetalleUsuario("ver_detalle_proveedor",nickname);
		});

	};	
 }

window.addEventListener("load",alCargarDocumento)