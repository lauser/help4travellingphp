function insertarActualizarReserva(idForm){

		    var formData = new FormData($(idForm)[0]);

            var ruta = "ajax.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    location.reload();
                    $("#respuesta").html(datos);
                }
            });			

}

function mostrarFormularioActualizarReserva(e){
		e.preventDefault();
		var nroAutoGenerado = e.target.dataset.numero;
		datos = "transaccion=mostrar_formulario_actualizar_estado_reserva&nroAuto="+nroAutoGenerado;
		var ruta = "ajax.php";
		$.ajax({
			url: ruta,
			type: "POST",
			data: datos,
			success: function(datos)
			{
			    $("#respuesta").html(datos);
				// document.querySelector("#alta-estado-reserva").addEventListener("submit",alert("ok"));	
				document.querySelector("#alta-estado-reserva").addEventListener("submit",function(evento){
					evento.preventDefault();
					insertarActualizarReserva("#alta-estado-reserva");
				});				
			}
		});			
} 


function alCargarDocumento(){

	var linksActualizarReserva = document.querySelectorAll(".actualizar-reserva");


			for ( var i = 0 ; i < linksActualizarReserva.length ;i++ ){

				linksActualizarReserva[i].addEventListener("click", mostrarFormularioActualizarReserva);
		
			};	


 }

window.addEventListener("load",alCargarDocumento)