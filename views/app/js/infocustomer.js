
function verDetalleUsuario(transaccion, nickname){

	datos = "transaccion="+transaccion+"&nickname="+nickname;
	var ruta = "ajax.php";
	$.ajax({
		url: ruta,
		type: "POST",
		data: datos,
		success: function(datos)
		{
		    $("#respuesta").html(datos);
		    $(window).scrollTop(0);
		}
	});		

}
						
function alCargarDocumento(){

		var linksDetalleCliente = document.querySelectorAll(".detalle-cliente");


		for ( var i = 0 ; i < linksDetalleCliente.length ;i++ ){

			linksDetalleCliente[i].addEventListener("click", function(e){
				e.preventDefault();
				var nickname = e.target.dataset.nickname;
				verDetalleUsuario("ver_detalle_cliente",nickname);
			});
	
		};	

 }

window.addEventListener("load",alCargarDocumento)