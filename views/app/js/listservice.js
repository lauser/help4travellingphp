function regService(e){

		e.preventDefault();
	    var formData = new FormData($("#actualizar-servicio")[0]);

        var ruta = "ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
                $("#respuesta").html(datos);
                // location.reload();
            }
        });

}

function mostrarFormularioActualizarServicio(evento){
		evento.preventDefault();
		//	Obtuvimos los valores de data-proveedor y data-servicio que definimos  
		//	en la funcion verListaServicios().
		// 	evento.target contiene el elemento html en el cual ocurrio el evento.
		// 	Debemos obtener ambos datos porque el id de la tabla servicios esta compuesta
		// 	por el nombre del proveedor y el nombre del servicio.
		var proveedor = evento.target.dataset.proveedor;
		var servicio = evento.target.dataset.servicio;
		datos = "transaccion=mostrar_formulario_actualizar_servicio&proveedor="+proveedor+"&servicio="+servicio;
		var ruta = "ajax.php";
		$.ajax({
			url: ruta,
			type: "POST",
			data: datos,
			success: function(datos)
			{
			    $("#respuesta").html(datos);
			    $(window).scrollTop(0);
				document.querySelector("#actualizar-servicio").addEventListener("submit",regService);	
			}
		});	


}


function alCargarDocumento(){
	
	var linksActualizarServicio = document.querySelectorAll(".actualizar-servicio");


	for ( var i = 0 ; i < linksActualizarServicio.length ;i++ ){

		linksActualizarServicio[i].addEventListener("click", mostrarFormularioActualizarServicio);

	};	

 }

window.addEventListener("load",alCargarDocumento)