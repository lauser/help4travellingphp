var respuesta = document.querySelector("#respuesta");
var cantClicksCDestino = 0;
var cantidadImagenes = 0;

function regService(e){

		e.preventDefault();
	    var formData = new FormData($("#alta-servicio")[0]);

        var ruta = "ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
            	console.log(datos);
                $("#respuesta").html(datos);
                // location.reload();
            }
        });

}

function mostrarCampo(id_opt, id, type, name, cantClicks, label_text){

			if (cantClicks == 0 ) {
				var padre = document.querySelector(id_opt);

				var label = document.createElement('LABEL');
				var texto = document.createTextNode(label_text);
				label.setAttribute("for", id);
				label.appendChild(texto);
				var item = document.createElement('INPUT');
				item.setAttribute("id",id);
				item.setAttribute("type", type);
				item.setAttribute("name",name);


				var br = document.createElement('br');
				padre.appendChild(br);			
				padre.appendChild(label)
				padre.appendChild(item);

			}

			if (cantClicks % 2 == 0 ) {
			    document.querySelector("#"+id).style.display = "block";
			}
			else{
				document.querySelector("#"+id).style.display = "none";
				

			}	
}

function mostrarCampoImagen(){
			// obtener el elemento con id mas_img del DOM es correcto 
			// porque el navegador ya renderizo la respuesta del servidor 
			cantidadImagenes++;
			var padre = document.querySelector("#mas_img").parentNode;
			if (cantidadImagenes < 4 ) {
				// Las siguientes 4 sentencias representan
				// "<input type='text' name = 'imagenes_servicios_txt[]' required>"
				var item_img = document.createElement('INPUT');
				item_img.setAttribute("type", "text");
				item_img.setAttribute("name","imagenes_servicios_txt[]");
				item_img.required = true;

				var br = document.createElement('br');

				padre.appendChild(item_img);
				padre.appendChild(br);
				
			}
			if (cantidadImagenes ==  4) {
				var avisoIMG = document.createTextNode("Solo puedes agregar 4 imagenes");
				padre.appendChild(avisoIMG);
			}

		}

function alCargarDocumento(){
	
	document.querySelector('#alta-servicio').addEventListener("submit", regService);
	// document.querySelector('#mas_img').addEventListener("click", mostrarCampoImagen);

	document.querySelector('#c_destino_opt').addEventListener("click",function(){

		mostrarCampo("#contenedor_cDestino","ciudad_destino","text", "ciudad_destino_txt", cantClicksCDestino, "");

		cantClicksCDestino++;
	});

 }

window.addEventListener("load",alCargarDocumento)
