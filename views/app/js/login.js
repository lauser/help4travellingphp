function sendDataLogin(e){

		e.preventDefault();
	    var formData = new FormData($("#login")[0]);

        var ruta = "ajax.php";
        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(datos)
            {
                if (datos.substring(0,5) == "Error") {

                    $("#respuesta").html(datos);
                    $("#respuesta").addClass('error');
                
                }else{
                    $("#cabecera").html(datos);
                    $("#login").css("display","none");
                    $("#respuesta").html("BIENVENIDO A HELP4TRAVELLING");
                    $("#respuesta").removeClass('error');
                    $("#respuesta").addClass('success');
                    // $(".contenido").addClass('contenedor-inicio');
                };
            }
        });	

}


function alCargarDocumento(){
	
	document.querySelector('#login').addEventListener("submit", sendDataLogin);

 }

window.addEventListener("load",alCargarDocumento)