-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 05-01-2018 a las 17:11:49
-- Versión del servidor: 5.5.58-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `help4travelling`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `nicknameC` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`nicknameC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`nicknameC`) VALUES
('elBicho'),
('eWatson'),
('laurex'),
('lauser'),
('marita'),
('oWood'),
('sergio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenesServicios`
--

CREATE TABLE IF NOT EXISTS `imagenesServicios` (
  `nicknameP` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombreServ` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagen` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`nicknameP`,`nombreServ`,`imagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `imagenesServicios`
--

INSERT INTO `imagenesServicios` (`nicknameP`, `nombreServ`, `imagen`) VALUES
('Genio', 'gol de tiro libre', '/home'),
('Genio', 'gol de tiro libre', '/var'),
('remus', 'Alquiler de auto', 'auto.jpg'),
('remus', 'Euro-Vuelo-LC', 'vuelo-lc.jpg'),
('remus', 'Euro-Vuelo-S', 'ruta-s-imagen1.jpeg'),
('remus', 'Euro-Vuelo-S', 'ruta-s-imagen2'),
('remus', 'Euro-Vuelo-S', 'ruta-s-imagen3'),
('remus', 'mars one', 'marte.jpg'),
('remus', 'velocidad luz', 'planeta.jpg'),
('remus', 'velocidad luz', 'planetak.jpg'),
('tCook', 'Air-France-FC', 'Air-France-FC.jpg'),
('tCook', 'Air-France-FC', 'Air-France-FC1.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `nickname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` int(10) unsigned NOT NULL DEFAULT '123',
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fechaNac` date NOT NULL,
  `imagen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'miPerfil.png',
  PRIMARY KEY (`nickname`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`nickname`, `password`, `nombre`, `apellido`, `email`, `fechaNac`, `imagen`) VALUES
('cc28', 123, 'gustavo', 'cabrera', 'gus@gmail.com', '2018-01-16', 'tareaCelery.png'),
('elBicho', 123, 'ronaldo', 'nazareno', 'r9@gmail', '1980-12-30', 'vuelo-lc.jpg'),
('eWatson', 123, 'Emma', 'Watson', 'e.watson@gmail.com', '1990-11-15', 'miPerfil.png'),
('Genio', 123, 'Andrea', 'Pirlo', 'a@gmail.com', '1978-06-14', 'Oliver_Wood.jpg'),
('larua', 123, 'laura', 'lopez', 'l@gmail.com', '2017-12-22', 'prueba.png'),
('laurex', 123, 'laur', 'lopez', 'l@hotmail.com', '2018-01-18', 'error_notificacion.png'),
('lauser', 123, 'sergio', 'cabrera', 'sergiogu.@gmail.com', '2017-12-22', 'CSS3.jpg'),
('marita', 123, 'marilyn', 'wong', 'm@hotmail.com', '2018-01-19', 'MTU.png'),
('moody', 123, 'Alastor', 'Moody', 'eu.car@eucar.com', '1989-10-05', 'miPerfil.png'),
('oWood', 123, 'Oliver', 'Wood', 'quidditch@gmail.com', '1975-11-26', 'miPerfil.png'),
('remus', 123, 'Remus', 'Lupin', 'iberia@gmail.com', '1980-11-22', 'miPerfil.png'),
('sergio', 123, 'cc', 'wong', 'w@gmail.xom', '2017-12-07', 'auto.jpg'),
('tCook', 123, 'Tim', 'Cook', 'air.f@gmail.com', '1980-11-14', 'miPerfil.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE IF NOT EXISTS `proveedor` (
  `nicknameP` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `empresa` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`nicknameP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`nicknameP`, `link`, `empresa`) VALUES
('cc28', 'sofis.com', 'sofis'),
('Genio', 'milan.com', 'milan'),
('larua', 'tiendabeautiful.com', 'bella'),
('moody', 'http://www.europcar.com.uy/', 'EuropCar'),
('remus', 'http://www.iberia.com/uy/', 'Iberia'),
('tCook', 'http://www.airfrance.com/', 'AirFrance');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reserva`
--

CREATE TABLE IF NOT EXISTS `reserva` (
  `nroAutogenerado` int(10) unsigned NOT NULL,
  `fechaCreacion` date NOT NULL,
  `precioTotal` float unsigned NOT NULL,
  `Estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`nroAutogenerado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reserva`
--

INSERT INTO `reserva` (`nroAutogenerado`, `fechaCreacion`, `precioTotal`, `Estado`) VALUES
(10, '2017-11-16', 1100, 'Registrada'),
(20, '2017-11-17', 200, 'Registrada'),
(30, '2017-11-05', 2200, 'Pagada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reservaDeServicios`
--

CREATE TABLE IF NOT EXISTS `reservaDeServicios` (
  `nroAuto` int(10) unsigned NOT NULL,
  `nicknameProveedor` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombreServicio` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nicknameCliente` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad` int(10) unsigned NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFin` date NOT NULL,
  PRIMARY KEY (`nroAuto`,`nicknameProveedor`,`nombreServicio`),
  KEY `fkreservaservicios` (`nicknameCliente`),
  KEY `fkreservaservicios_di2` (`nicknameProveedor`,`nombreServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reservaDeServicios`
--

INSERT INTO `reservaDeServicios` (`nroAuto`, `nicknameProveedor`, `nombreServicio`, `nicknameCliente`, `cantidad`, `fechaInicio`, `fechaFin`) VALUES
(10, 'remus', 'Euro-Vuelo-LC', 'oWood', 1, '2018-01-17', '2018-01-20'),
(10, 'remus', 'Euro-Vuelo-S', 'oWood', 1, '2017-11-24', '2017-11-25'),
(20, 'tCook', 'Air-France-FC', 'oWood', 2, '2017-12-31', '2018-01-01'),
(30, 'remus', 'Euro-Vuelo-S', 'eWatson', 2, '2018-03-21', '2018-03-23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE IF NOT EXISTS `servicios` (
  `nicknameProveedor` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombreServicio` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` float unsigned NOT NULL,
  `ciudadOrigen` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ciudadDestino` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`nicknameProveedor`,`nombreServicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`nicknameProveedor`, `nombreServicio`, `descripcion`, `precio`, `ciudadOrigen`, `ciudadDestino`) VALUES
('Genio', 'gol de tiro libre', 'Gol garantizado  para distancias menores a 30 metros', 1000000, 'Roma', ''),
('remus', 'Alquiler de auto', 'blbalb', 15500, 'Montevideo', ''),
('remus', 'Euro-Vuelo-LC', 'Vuelo con excelente atención y comodidad a un precio accesible.', 10000, 'montevideo', 'valencia'),
('remus', 'Euro-Vuelo-S', 'Vuelo con excelente atención y comodidad.', 1100, 'montevideo', 'valencia'),
('remus', 'mars one', 'viaje hacia el planeta rojo', 15000, 'londres', ''),
('remus', 'velocidad luz', 'viaje al planeta k205 a la velocidad del sonido', 10000000, 'paris', ''),
('tCook', 'Air-France-FC', '¡Un vuelo de primera! Excelencia y experiencia en mejorar sus viajes"', 100, 'Paris', 'Berlin');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`nicknameC`) REFERENCES `persona` (`nickname`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `imagenesServicios`
--
ALTER TABLE `imagenesServicios`
  ADD CONSTRAINT `fkimagenesServicios` FOREIGN KEY (`nicknameP`, `nombreServ`) REFERENCES `servicios` (`nicknameProveedor`, `nombreServicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`nicknameP`) REFERENCES `persona` (`nickname`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `reservaDeServicios`
--
ALTER TABLE `reservaDeServicios`
  ADD CONSTRAINT `fkreservaservicios` FOREIGN KEY (`nicknameCliente`) REFERENCES `cliente` (`nicknameC`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkreservaservicios_di1` FOREIGN KEY (`nroAuto`) REFERENCES `reserva` (`nroAutogenerado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkreservaservicios_di2` FOREIGN KEY (`nicknameProveedor`, `nombreServicio`) REFERENCES `servicios` (`nicknameProveedor`, `nombreServicio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD CONSTRAINT `servicios_ibfk_1` FOREIGN KEY (`nicknameProveedor`) REFERENCES `proveedor` (`nicknameP`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
