<?php 

$nombre_servicio = $_POST["nombre_servicio_txt"];
$descripcion_servicio =  $_POST["descripcion_servicio_txa"];
$imagenes_servicios = $_POST["imagenes_servicios_txt"];
$precio_servicio = $_POST["precio_servicio_txt"];
$ciudad_origen =  $_POST["ciudad_origen_txt"];
$proveedor = $_POST["proveedor_hd"];
$nombre_servicio_viejo = $_POST["servicio_hd"];

// Al actualizar el nombre de servicio en la tabla servicios se realiza automaticamente una
  // actualizacion del atributo nombreSer de la tabla imagenesServicio . Por lo tanto la actualizacion de las imagenes del servicio debe hacerse consultando con el nuevo nombre de servicio .
  $mysql = conexionMySQL(); 
  $sql_datos = "UPDATE servicios 
                SET nombreServicio = '$nombre_servicio', 
                    descripcion = '$descripcion_servicio',
                    precio = '$precio_servicio',
                    ciudadOrigen = '$ciudad_origen' 
                WHERE nicknameProveedor = '$proveedor' AND nombreServicio= '$nombre_servicio_viejo'  ";

  if ($resultado = $mysql->query($sql_datos)) {

          $respuesta = "<div class='alert alert-success' data-recargar>
                          se actualizo con exito los datos del servicio : <b>$nombre_servicio</b>
                        <div>";
    
  }
  else{

    $respuesta = "<div class='alert alert-danger'>
                      Ocurrio un error no se actualizo el registro del servicio:
                       <b>$nombre_servicio</b>
                  </div>";

  };

  $sql_obtener_parte_clave = "SELECT * 
                              FROM  imagenesServicios 
                              WHERE nicknameP = '$proveedor' AND nombreServ= '$nombre_servicio'";

  if ($resultado = $mysql->query($sql_obtener_parte_clave)) {

        $i = 0;
        while ($fila = $resultado->fetch_assoc()) {

          $sql_img = "UPDATE imagenesServicios 
                      SET imagen = '$imagenes_servicios[$i]' 
                      WHERE nicknameP = '$proveedor' AND nombreServ = '$nombre_servicio' AND imagen =  '".$fila["imagen"]."' ";

          if (!($resultado_img = $mysql->query($sql_img))) {  

             $respuesta .= "<div class='alert alert-danger'>
                               Ocurrio un error no se actualizo la imagen del servicio:
                               <b>".$imagenes_servicios[$i]."</b>
                           </div>";
          }

          $i++;             
        };
                     
   
   } 
   else{

    $respuesta = "<div class='alert alert-danger'>
                     No se pudo obtener parte de la clave primaria de la tabla imagenesServicios
                 </div>";

   }                        
                              

  $mysql->close();

  printf($respuesta);


 ?>