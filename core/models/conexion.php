<?php 
	
	// require_once "config.php"; //  esta forma es la mas eficiente de invocar archivos 
	
	function conexionMySQL(){

		$conexion = new mysqli(SERVER,USER,PASS,BD);
		if($conexion->connect_error){
			

				$error ="<div class = 'error'> error de conexion nro :<b>%d</b> Mensaje de error : <mark> %s</mark></div>";
				printf($error,$conexion->connect_errno,$conexion->connect_error);
				die($error);
		}else{
			// $formato = "<div class = 'mensaje'>Conexion exitosa:<b>". $conexion->host_info."</b></div>";
			// echo $formato;

			//$formato = "<div class = 'mensaje'>Conexion exitosa:<b>%s</b></div>";
			//($formato,$conexion->host_info);
		}

		$conexion->query("SET CHARACTER SET UTF8");// es para que toda la informacion que traiga php desde la base de datos se muestre los datos con tildes 

		return $conexion;
	}
 //invoco a la funcion definida en este archivo
	// conexionMySQL();
 ?>