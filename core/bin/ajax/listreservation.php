<?php 
function  mostrar_f_actualizar_estado_reserva($nroAuto){
	
	$mysql= conexionMySQL();
	$sql =  "SELECT * FROM reserva WHERE nroAutogenerado = '$nroAuto'";
	$resultado = $mysql->query($sql);
	if ($resultado) {
		$fila = $resultado->fetch_assoc();
		$registrada = "";
		$cancelada="";
		$pagada = "";
		$facturada = "";

		if ($fila["Estado"] == "REGISTRADA") {

			$registrada = "selected";
		
		} elseif ($fila["Estado"] =="CANCELADA") {
		
			$cancelada="selected";
		
		} elseif ($fila["Estado"] =="PAGADA") {
		
			$pagada = "selected";
		
		}elseif ($fila["Estado"] =="FACTURADA") {
			
			$facturada = "selected";
		}

		$form="<form id='alta-estado-reserva' class='formulario' >";
			$form.="<fieldset>";
				$form.="<legend>ACTUALIZAR ESTADO RESERVA</legend>";
				$form.="<div>";
					 $form.="nroAutogenerado : ".$fila["nroAutogenerado"];
				$form.="</div>";
				$form.="<div>";
					$form.="Precio total : ".$fila["precioTotal"];
				$form.="</div>";
				$form.="<div>";
					$form.="<label for = 'estado'>Estado </label>";
					if ($fila["Estado"]=="REGISTRADA") {
						$form.="<select id='estado' name = 'estado_sl'>";
							$form.="<option value='REGISTRADA'".$registrada.">REGISTRADA</option>";
							$form.="<option value='CANCELADA' ".$cancelada.">CANCELADA</option>";
							$form.="<option value='PAGADA' ".$pagada.">PAGADA</option>";
							$form.="<option value='FACTURADA ".$facturada."'>FACTURADA</option>";
						$form.="</select>";						
					} else {
						$form.="Estado : ".$fila["Estado"]."<br>";
						$form.="<strong>Solo se pueden modificar las reservas con estado REGISTRADA</strong>";
					};
					
;
				$form.="</div>";
				$form.="<div>";
					$form.=	"<input type='submit' id = 'actualizar_reserva' class='submit' name='actualizar_reserva_btn' value = 'ACTUALIZAR'>";
					$form.=	"<input type='hidden' id ='transaccion' name='transaccion' value = 'actualizar_reserva'>";
					$form.=	"<input type='hidden' name='nroAutogenerado' value = '".$fila["nroAutogenerado"]."'>";									
				$form.="</div>";

			$form.="</fieldset>";
		$form.="</form>";

		printf($form);		

	} else {

		printf("ocurrio un error al ejecutar la consulta en la tabla reserva");

	}
	
}

mostrar_f_actualizar_estado_reserva($_POST["nroAuto"]);
 ?>