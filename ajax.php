<?php 

if ($_POST) {

    require('core/core.php');

    switch (isset($_POST['transaccion']) ? $_POST['transaccion'] : null ) {
    	case 'insertar_usuario':
    		require('core/models/registroUser.php');
    		break;
    	case 'insertar_servicio':
            require('core/models/regservice.php');
            break;
        case 'mostrar_formulario_actualizar_servicio':
            require('core/bin/ajax/listservice.php');
            break;
        case 'actualizar_servicio':  
            require('core/models/updateservice.php');
            break; 
        case 'insertar_reserva':
            require('core/models/reservation.php');
            break;       
        case 'mostrar_servicios_proveedor':  
            require('core/bin/ajax/showservice.php');
            break;
        case 'mostrar_formulario_actualizar_estado_reserva':
            require('core/bin/ajax/listreservation.php');
            break;       
        case 'actualizar_reserva':
            require('core/models/updatereservation.php');           
            break;
        case 'eliminar_reserva':
            require('core/models/deletereservation.php');           
         break;
        case 'ver_detalle_cliente':
            require('core/bin/ajax/infocustomer.php');    
            break;                                  
        case 'ver_detalle_proveedor':
            require('core/bin/ajax/infoprovider.php');    
            break;                   

        case 'detalle_servicio':
            require('core/bin/ajax/infoservice.php');    
            break;

        case 'detalle_reserva':
            require('core/bin/ajax/inforeservation.php');    
            break;
        case 'login':
            require('core/bin/ajax/login.php');
            break;                        
    	default:
			header('location: index.php');	
    		break;
    }

} else {

	header('location: index.php');	

}


 ?>