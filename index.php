<?php 
require('core/core.php');


session_start();

if (isset($_GET["view"]) && isset($_SESSION["admin"])) {

	if (file_exists('core/controllers/'.strtolower($_GET["view"]).'Controller.php')) {

			include('core/controllers/'.strtolower($_GET["view"]).'Controller.php');
	
	} else {
		
		include('core/controllers/errorController.php');		
	}
	

}else{
	include('core/controllers/loginController.php');


}

 ?>