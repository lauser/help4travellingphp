<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php printf(APP_TITLE) ?></title>
	<meta name="description" content="Aplicacion Help4travelling(Create-read-update-delete) con filosofia MVC desarrollada en php, mysql y ajax">
	<link rel="stylesheet" href="views/app/css/main.css">
	<link rel="stylesheet" href="views/app/css/hint.min.css">
	<meta name = "viewport" content = "width=device-width, user-scalable = no, initial-scale = 1.0, maximun-scale = 1.0,minimum-scale = 1.0">
	<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> -->

    <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->

</head>