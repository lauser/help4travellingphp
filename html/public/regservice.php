
<?php include(HTML_DIR.'overall/header.php') ?>
<?php include(HTML_DIR.'overall/topnav.php') ?>

	

<section class="contenido">
	<div id="respuesta">
	</div>
	<div id="precarga"></div>

	<div>
			<h2>Informacion de los campos del servicio</h2>

			<div class='hint--bottom-right more-info' data-hint='Elige hasta un máximo de 4 imagenes en formato png,  jpeg o jpg.'>
		    	Acerca de las imagenes del servicio.
			</div>
			<br>
			<div class='hint--bottom-right more-info' data-hint='El nickname del proveedor debe estar almacenado en la base de datos previamente. En caso que no este debes dar de alta a dicho proveedor.'>
				Acerca del campo Proveedor.
			</div>
			<br>
			<div class='hint--bottom-right more-info' data-hint='Debes marcar la casilla Ciudad destino en el caso que el servicio sea de transporte, envio, etc.'>
				Acerca del campo Ciudad de destino.
			</div>
	</div>

	 <form id = 'alta-servicio' class = 'formulario' data-insertarServicio >
		<fieldset>
			<legend>ALTA DE SERVICIO</legend>
					<div>
						<label for = 'nombre_servicio'> Nombre servicio: </label>
						<input type='text' id = 'nombre_servicio' name='nombre_servicio_txt' required>
					</div>
					<div>
						<label for = 'descripcion_servicio'> Descripcion de servicio: </label>
						<textarea id = 'descripcion_servicio' name = 'descripcion_servicio_txa' required></textarea>
					</div>
<!-- 					<div>
			    	 <label for ='imagenes_servicios'> Imagenes del servicio: </label>
			    	 <input type='text' id = 'imagenes_servicios' name = 'imagenes_servicios_txt[]' required>
			    	 <button type = 'button' id = 'mas_img' data-mostrarimg > + </button><br>
				 </div> -->
				<div>
					<label for = 'imagenes_servicios_servicio'> Imagenes del servicio: </label>
					<input type= 'file' id='imagenes_servicios' name = 'imagenes_servicios_txt[]' accept='image/png, .jpeg, .jpg' multiple>
				</div>				 
				<div>
					<label for = 'precio_servicio'>Precio del servicio: </label>
					<input type= 'text' id= 'precio_servicio' name='precio_servicio_txt' required>
				</div>				
					<div>
					<label for = 'ciudad_origen'>Ciudad origen: </label>
					<input type='text'  id = 'ciudad_origen' name='ciudad_origen_txt' required>
				</div>
					<div>
						<label for = 'proveedor_servicio'>Nickname de 	Proveedor:</label>
						<input type='text' id = 'proveedor_servicio' name='proveedor_servicio_txt' required>
				</div>				

					<div>
					<label for = 'c_destino_opt' data-mostrarCDestino> Ciudad Destino: </label>
					<input type= 'checkbox' id='c_destino_opt' name = 'ciudad_destino_chk' value=''> 
				</div>

				<div id= 'contenedor_cDestino'>
				</div>
				
				<div>
					<input class='submit' type='submit' id = 'insertar_servicio' name='insertar_servicio_btn' value = 'INSERTAR'>
					<input type='hidden' id ='transaccion' name='transaccion' value = 'insertar_servicio'>					
				</div>				
		</fieldset>	
	</form>	

</section>	

<script  src= "views/app/js/regservice.js"></script>
	
<?php include(HTML_DIR.'overall/footer.php') ?>