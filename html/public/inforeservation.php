<?php include(HTML_DIR.'overall/header.php') ?>
<?php include(HTML_DIR.'overall/topnav.php') ?>

<?php 
function ver_lista_reservas($data, $clase, $text_enlace){
	$mysql = conexionMySQL();
	$sql = "SELECT nroAutogenerado, precioTotal, Estado FROM reserva";
	$resultado = $mysql->query($sql);

	if ($resultado) {
	    $lista="<div class = 'list list-head'>";
					$lista.= "<div>";
						$lista.= "RESERVA";
					$lista.= "</div>";	
					$lista.= "<div>";
						$lista.="PRECIO TOTAL";
					$lista.= "</div>";	
					$lista.= "<div>";
						$lista.= "ESTADO";
					$lista.= "</div>";
					$lista.="<div>";
						$lista.="ACCION";
					$lista.="</div>";		 	
		$lista.="</div>";
		$lista.="<hr>";
		while ($fila = $resultado->fetch_assoc()) {
			$lista.="<div class = 'list' data-$data>";
				$lista.= "<div>";
					$lista.= $fila["nroAutogenerado"];
				$lista.= "</div>";	
				$lista.= "<div>";
					$lista.= $fila["precioTotal"];
				$lista.= "</div>";	
				$lista.= "<div>";
					$lista.= $fila["Estado"];
				$lista.= "</div>";

				$lista.= "<div>";
					$lista.="<a class = '$clase' href='#' data-numero='".$fila["nroAutogenerado"]."'> $text_enlace </a>";
				$lista.="</div>";
			$lista.="</div>";
			$lista.="<hr>";
		}
	
	} else {

		return printf("no se pudo obtener los datos de la tabla reserva");
	}

	printf($lista);
}    


 ?>
		<section class="contenido">
				<div id="respuesta">
				</div>
				<div id="precarga"></div>			
				<?php     ver_lista_reservas("seleccionarreserva","seleccionar-reserva","seleccionar");?>			

	   </section>	

   <script  src= "views/app/js/inforeservation.js"></script>
	
<?php include(HTML_DIR.'overall/footer.php') ?>		

