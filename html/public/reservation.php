<?php include(HTML_DIR.'overall/header.php') ?>
<?php include(HTML_DIR.'overall/topnav.php') ?>	
<?php  
function ver_usuarios_reserva($nombre_tabla,$columna,$id_select,$text_select, $name_select){
	$mysql = conexionMySQL();
	$sql="SELECT $columna FROM $nombre_tabla";
	if($resultado = $mysql->query($sql)){

		$lista ="<label for = '".$id_select."'>".$text_select."</label>"; 
			$lista.="<select id = '".$id_select."' name = '".$name_select."'>"; 
				$lista.="<option value=''>---</option>"; 
		while ($fila = $resultado->fetch_assoc()){
				       
		  $lista .="<option value='".$fila[$columna]."'>".$fila[$columna]."</option>";
		
		};		
		$lista.="</select>"; 

	}else{

		return printf("error");
	
	}
	$resultado->free();
	$mysql->close();
	return $lista;
};


?>
	<section class="contenido">
		<div id="respuesta">
		</div>
		<div id="precarga"></div>		
		<div>
				<h2>Informacion de los campos de la reserva</h2>
				<div class='hint--bottom-right more-info' data-hint='Indica el precio total de la reserva'>
			    	Acerca del precio.
				</div>

				<br>
				<div class='hint--bottom-right more-info' data-hint='Seleccionar cualquier proveedor. Una ves seleccionado se mostrara los servicios asociados a dicho proveedor'>
			    	Acerca del proveedor seleccionado.
				</div>
				<br>
				<div class='hint--bottom-right more-info' data-hint='Representa la cantidad a reservar de un mismo servicio.'>
			    	Acerca del Campo cantidad.
				</div>

		</div>					
<?php 
		$form  = "<form id= 'alta-reserva' class='formulario' data-altaReserva>";
			$form.="<fieldset>"; 
				$form.="<legend>ALTA RESERVA</legend>"; 
				$form.=	"<div>";
					$form.=	"<label for = 'precio_total'> Precio_total: </label>";
					$form.=	"<input type='text' id = 'precio_total' name='precio_total_txt' required>";
				$form.=	"</div>";		
				$form.="<div>"; 
					$form.=ver_usuarios_reserva("cliente","nicknameC","cliente_r","Elegir Cliente :", "cliente_reserva_sl"); 
				$form.="</div>"; 
				$form.="<div>";
					$form.=ver_usuarios_reserva("proveedor","nicknameP","proveedor_r","Elegir Proveedor :","proveedor_reserva_sl");
				$form.="</div>";	
				$form.="<div id='lista_servicios_proveedor'>"; 

				$form.="</div>";
				 
				$form.=	"<div>";
					$form.=	"<label for = 'cantidad'> Cantidad: </label>";
					$form.=	"<input type='text' id = 'cantidad' name='cantidad_txt' required>";
				$form.=	"</div>";
				$form.=	"<div>";
						$form.="<label for = 'fecha_inicio'>Fecha inicio:</label>";
						$form.="<input type='date' id = 'fecha_incio' name='fecha_inicio_date' required>";
				$form.="</div>";
				$form.=	"<div>";
						$form.="<label for = 'fecha_fin'>Fecha fin:</label>";
						$form.="<input type='date' id = 'fecha_fin' name='fecha_fin_date' required>";
				$form.="</div>";							
				$form.="<div>";
					$form.="<input type='submit' id = 'insertar-reserva' class='submit' name='insertar-reserva-btn' value = 'INSERTAR'>";
					$form.="<input type='hidden' id ='transaccion' name='transaccion' value = 'insertar_reserva'>";					
				$form.="</div>";	
			$form.="</fieldset>"; 
		$form.="</form>"; 

		 printf($form);


 ?>			

	   </section>	

   <script  src= "views/app/js/reservation.js"></script>
	
<?php include(HTML_DIR.'overall/footer.php') ?>	 	