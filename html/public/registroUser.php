<?php include(HTML_DIR.'overall/header.php') ?>
<?php include(HTML_DIR.'overall/topnav.php') ?>	 

<section class = "contenido">
	<div id="respuesta">
		
	</div>
	<div id="precarga">
		
	</div>		
	<div class="">
		<h2>Informacion de los campos de alta un usuario</h2>

		<div class='hint--bottom-right more-info' data-hint='El valor de este campo debe ser único'>
	    	Campo Nickname 
		</div>
		<br>
		<div class='hint--bottom-right more-info' data-hint='La imagen debe estar en formato .jpeg, jpg o png.'>
	    	Campo Imagen
		</div>
		<br>
		<div class='hint--bottom-right more-info' data-hint='Al marcar la casilla se mostraran los campos link y empresa'>
	    	Campo Proveedor
		</div>					
	</div>
	 <form id = 'alta-usuario' class = 'formulario' enctype='multipart/form-data' data-altaUsuario>
		<fieldset>
			<legend>ALTA DE USUARIO </legend>
					<div>
						<label for = 'nickname'> Nickname: </label>
						<input type='text' id = 'nickname' name='nickname_txt' required>
					</div>
					<div>
						<label for = 'nombre'> Nombre: </label>
						<input type = 'text' id = 'nombre' name = 'nombre_txt' required>
					</div>
					<div>
						<label for = 'apellido'> Apellido: </label>
						<input type = 'text' id = 'apellido' name = 'apellido_txt' required>
					</div>	
					<div>
						<label for = 'correo-electronico'> E-mail: </label>
						<input type = 'email' id = 'correo-electronico' name = 'correo-electronico_eml' required>
					</div>		

					<div>
						<label for = 'fecha_nacimiento'>fecha nacimiento:</label>
						<input type='date' id = 'fecha_nacimiento' name='fecha_nacimiento_date' required>
				</div>						

					<div>
					<!-- <label for = 'img_usuario_opcional'> Imagen: </label> -->
					<input type= 'file' id='img_usuario_opcional' name = 'img_usuario_opcional_f' accept='image/png, .jpeg, .jpg'> 
				</div>
					<div>
					<label for = 'proveedor_opt' data-mostrarProveedor> Proveedor: </label>
					<input type= 'checkbox' id='proveedor_opt' name = 'proveedor_opt_chk'> 
				</div>
				<div id = 'contenedor_proveedor_link'>

				</div>
				<div id = 'contenedor_proveedor_empresa'>

				</div>				
				<div>
					<input type='submit' id = 'insertar-usuario' class='submit' name='insertar-usuario-btn' value = 'INSERTAR'>
					<input type='hidden' id ='transaccion' name='transaccion' value = 'insertar_usuario'>					
				</div>				
		</fieldset>	
	</form>			

</section>	

   <script  src= "views/app/js/registroUser.js"></script>
	
<?php include(HTML_DIR.'overall/footer.php') ?>	 

